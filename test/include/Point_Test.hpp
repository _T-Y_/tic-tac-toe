#ifndef POINT_TEST
#define POINT_TEST

#include <memory>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

    class Point;

    class Point_Test : public CppUnit::TestCase{  
        CPPUNIT_TEST_SUITE( Point_Test );
        CPPUNIT_TEST( test_Point1 ) ;
        CPPUNIT_TEST( test_Point2 ) ;
        CPPUNIT_TEST_SUITE_END();

        // Méthodes
        public:
        void setUp();
        void tearDown();
        void test_Point1();
        void test_Point2();

        // Atributs
        private:
        Point *p1;
        Point *p2;     
    };

#endif