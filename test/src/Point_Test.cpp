#include "../include/Point_Test.hpp"
#include "../../include/Point.hpp"

// Enregistrement des différents cas de tests
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( Point_Test, "Point_Test" );

void Point_Test::setUp(){
    this->p1 = new Point(2,3);
    this->p2 = new Point(4,5);
}

void Point_Test::tearDown(){
    delete this->p1;
    delete this->p2;
}

void Point_Test::test_Point1(){
    CPPUNIT_ASSERT(this->p1->getX() == 2);
    CPPUNIT_ASSERT(this->p1->getY() == 3);
}

void Point_Test::test_Point2(){
    CPPUNIT_ASSERT(this->p2->getX() == 4);
    CPPUNIT_ASSERT(this->p2->getY() == 5);
}